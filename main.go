package main

import (
	"net/http"
	"os"

	"gitlab.com/chief-api/api/httphandlers"
	"gitlab.com/chief-api/api/router"
	"gitlab.com/chief-api/pkg/loggr"
)

var (
	port string
)

func init() {
	port = os.Getenv("PORT")
	if len(port) == 0 {
		port = "3000"
	}
	port = ":" + port
}

func main() {
	l := loggr.New()
	h := httphandlers.New(l)
	r := router.NewRouter(h)

	l.Logger.Infof("Chief running at http://localhost%s", port)
	if err := http.ListenAndServe(port, r); err != nil {
		l.Logger.Fatal(err.Error())
	}
}
