package httphandlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/chief-api/api/hardwr"
	"gitlab.com/chief-api/pkg/encodr"
)

// ListWatchers will list all devices categorized as Watchers
func (h *Handler) ListWatchers(w http.ResponseWriter, r *http.Request) {
	watchersSlice := make([]*hardwr.Watcher, 0)

	for _, watcher := range watchers {
		watchersSlice = append(watchersSlice, watcher)
	}

	encodr.JSON(w, http.StatusOK, watchersSlice)
}

// ReadWatcher triggers the *Watcher.Read method
func (h *Handler) ReadWatcher(w http.ResponseWriter, r *http.Request) {
	mac := mux.Vars(r)["mac"]

	watcher, ok := watchers[mac]
	if !ok {
		encodr.JSONErr(w, http.StatusNotFound, http.StatusText(http.StatusNotFound))
		return
	}

	data, err := watcher.Read()
	if err != nil {
		encodr.JSONErr(w, http.StatusServiceUnavailable, "Failed to read")
		return
	}

	encodr.JSON(w, http.StatusOK, data)
}
