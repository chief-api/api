package httphandlers

import (
	"net/http"

	"gitlab.com/chief-api/pkg/encodr"
	"gitlab.com/chief-api/pkg/loggr"
)

// Handler holds a logger so the HandlerFuncs can access it
type Handler struct {
	Loggr *loggr.Loggr
}

// New instanciates a new handler
func New(loggr *loggr.Loggr) *Handler {
	return &Handler{loggr}
}

// NotFound replaces the standard mux.Router Handler that responds to Not Found
func (h *Handler) NotFound(w http.ResponseWriter, r *http.Request) {
	encodr.JSONErr(
		w,
		http.StatusNotFound,
		http.StatusText(http.StatusNotFound),
	)
}

// MethodNotAllowed replaces the standard mux.Router Handler that responds to Method Not Allowed
func (h *Handler) MethodNotAllowed(w http.ResponseWriter, r *http.Request) {
	encodr.JSONErr(
		w,
		http.StatusMethodNotAllowed,
		http.StatusText(http.StatusMethodNotAllowed),
	)
}
