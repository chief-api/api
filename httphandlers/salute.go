package httphandlers

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/chief-api/api/hardwr"
	"gitlab.com/chief-api/pkg/encodr"
)

var (
	boards   = make(hardwr.Boards, 0)
	watchers = make(hardwr.Watchers, 0)
	workers  = make(hardwr.Workers, 0)
)

// specialize sorts the devices into its specialized type
func specialize(board *hardwr.Board) error {
	switch board.Type {
	case hardwr.WatcherType:
		watchers[board.MAC] = &hardwr.Watcher{Board: board}
	case hardwr.WorkerType:
		workers[board.MAC] = &hardwr.Worker{Board: board}
	default:
		return errors.New("Not a known device type")
	}

	return nil
}

// Salute identifies the devices that make a POST to the API
func (h *Handler) Salute(w http.ResponseWriter, r *http.Request) {
	board := new(hardwr.Board)

	if err := json.NewDecoder(r.Body).Decode(&board); err != nil {
		encodr.JSONErr(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	defer r.Body.Close()

	// Check if board is already known, if so, update it's IP and URL
	if brd, ok := boards[board.MAC]; ok {
		brd.IP = board.IP
		brd.URL = board.URL
		encodr.JSON(w, http.StatusOK, brd)
		return
	}
	boards[board.MAC] = board
	go board.KeepAlive()

	if err := specialize(board); err != nil {
		encodr.JSONErr(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	encodr.JSON(w, http.StatusCreated, board)
}
