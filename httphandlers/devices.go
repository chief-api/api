package httphandlers

import (
	"net/http"

	"gitlab.com/chief-api/api/hardwr"
	"gitlab.com/chief-api/pkg/encodr"
)

// ListDevices returns a list of known devices
func (h *Handler) ListDevices(w http.ResponseWriter, r *http.Request) {
	devices := make([]*hardwr.Board, 0)

	for _, board := range boards {
		devices = append(devices, board)
	}

	encodr.JSON(w, http.StatusOK, devices)
}
