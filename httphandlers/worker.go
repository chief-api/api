package httphandlers

import (
	"fmt"
	"net/http"

	"gitlab.com/chief-api/api/hardwr"
	"gitlab.com/chief-api/pkg/encodr"
)

// ListWorkers will list all devices categorized as Workers
func (h *Handler) ListWorkers(w http.ResponseWriter, r *http.Request) {
	workersSlice := make([]*hardwr.Worker, 0)

	for _, worker := range workers {
		workersSlice = append(workersSlice, worker)
	}

	encodr.JSON(w, http.StatusOK, workersSlice)
}

// ExecuteTask do nothing for now
func (h *Handler) ExecuteTask(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Not implemented")
}
