package encodr

import (
	"encoding/json"
	"net/http"
)

const (
	contentType = "Content-Type"
	appJSON     = "application/json; charset=utf-8"
)

// encodeToJSON encodes a payload to JSON and sends it.
func encodeToJSON(w http.ResponseWriter, code int, payload interface{}) {
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(code)
	encodr := json.NewEncoder(w)
	encodr.SetIndent("", "  ")
	encodr.Encode(payload)
}

// JSON sends a JSON containing the payload
func JSON(w http.ResponseWriter, code int, payload interface{}) {
	encodeToJSON(w, code, payload)
}

// JSONErr sends a JSON containing the status code and a message in case of an error occurs
func JSONErr(w http.ResponseWriter, code int, msg string) {
	encodeToJSON(w, code, Error{code, msg})
}
