package encodr

// Error represents a error message containing the http code and a custom message
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
