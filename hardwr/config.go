package hardwr

const (
	StatusBusy        = "Busy"
	StatusIdle        = "Idle"
	StatusDisabled    = "Disabled"
	StatusUnreachable = "Unreachable"

	WatcherBoards = "watchers"
	WorkerBoards  = "workers"

	WatcherType = "Watcher"
	WorkerType  = "Worker"
)
