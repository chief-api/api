package hardwr

import "time"

// Worker represents a specialized board
// Workers are responsible for taking actions like turnig the lights off, etc
type Worker struct {
	*Board
}

// Workers represents a "group" of Worker devices
type Workers map[string]*Worker

// ExecuteTask does nothing for now
func (wrkr *Worker) ExecuteTask() {
	wrkr.Status = "Busy"
	time.Sleep(5 * time.Second)
	wrkr.Status = "Idle"
}
