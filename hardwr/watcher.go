package hardwr

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Watcher represents a specialized Board
// Watchers can only respond requests with data like weather, energy consumption, etc
type Watcher struct {
	*Board
}

// Watchers represents a "group" of Watcher devices
type Watchers map[string]*Watcher

// Read sends a HTTP GET to watcher.URL+/read e.g. http://192.168.100.101/read
// and returns a map containing the read data.
func (wtchr *Watcher) Read() (map[string]interface{}, error) {
	var data map[string]interface{}

	resp, err := http.Get(wtchr.URL + "/read")
	if err != nil {
		return nil, err
	}

	readBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err := json.Unmarshal(readBytes, &data); err != nil {
		return nil, err
	}

	return data, nil
}
