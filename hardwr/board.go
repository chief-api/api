package hardwr

import (
	"net/http"
	"time"
)

// Board represents a physical device and its attributes
type Board struct {
	*NetInterface `json:"net_interface"`
	URL           string   `json:"url"`
	Version       string   `json:"version"`
	Model         string   `json:"model"`
	Type          string   `json:"type"`
	Status        string   `json:"status"`
	Routes        []string `json:"routes"`
}

// Boards represents a "group" of Board
type Boards map[string]*Board

// KeepAlive sends a HTTP GET every 5 seconds,
// if something wrong occurs, the Board status is
// changed to Unreachable
//
// TODO: Attributing "StatusIdle" for the Board status
// everytime the HTTP GET is successful is a bad idea
// because it will "shadow" the true board's status
// Maybe its better to request the status from
// the board itself.
func (brd *Board) KeepAlive() {
	for {
		if _, err := http.Get(brd.URL + "/keepalive"); err != nil {
			brd.Status = StatusUnreachable
		} else {
			brd.Status = StatusIdle
		}
		time.Sleep(5 * time.Second)
	}
}
