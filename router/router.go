package router

import (
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"gitlab.com/chief-api/api/httphandlers"
)

var (
	originsOk = handlers.AllowedOrigins([]string{"*"})
	methodsOk = handlers.AllowedMethods([]string{"GET", "POST"})
)

// NewRouter initializes the router with CORS and necessary routes
func NewRouter(h *httphandlers.Handler) http.Handler {
	// Creating a expansible middleware chain, for logging, authorization, etc
	middlewrs := alice.New(h.Loggr.Log)

	// Necessary to match encode url. The board mockage uses encoded url
	r := mux.NewRouter().UseEncodedPath()

	// Configuring handlers for NotFound and MethodNotAllowed
	r.NotFoundHandler = middlewrs.ThenFunc(h.NotFound)
	r.MethodNotAllowedHandler = middlewrs.ThenFunc(h.MethodNotAllowed)

	// Identification route
	r.Handle("/salute", middlewrs.ThenFunc(h.Salute)).Methods("POST")

	// Devices routes
	r.Handle("/devices", middlewrs.ThenFunc(h.ListDevices)).Methods("GET")

	// Watchers routes
	r.Handle("/devices/watchers/", middlewrs.ThenFunc(h.ListWatchers)).Methods("GET")
	r.Handle("/devices/watchers/{mac}/read", middlewrs.ThenFunc(h.ReadWatcher)).Methods("GET")

	// Workers routes
	r.Handle("/devices/workers/", middlewrs.ThenFunc(h.ListWorkers)).Methods("GET")

	return handlers.CORS(originsOk, methodsOk)(r)
}
